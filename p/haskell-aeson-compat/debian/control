Source: haskell-aeson-compat
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.7.0.6),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-attoparsec-dev (>= 0.12),
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-prof,
 libghc-attoparsec-iso8601-dev (>= 1.0.0.0),
 libghc-attoparsec-iso8601-dev (<< 1.1),
 libghc-attoparsec-iso8601-prof,
 libghc-base-compat-dev (>= 0.6.0),
 libghc-base-compat-dev (<< 0.11),
 libghc-base-compat-prof,
 libghc-base-orphans-dev (>= 0.4.5),
 libghc-base-orphans-dev (<< 0.9),
 libghc-exceptions-dev (>= 0.8),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof,
 libghc-hashable-dev (>= 1.2),
 libghc-hashable-dev (<< 1.4),
 libghc-hashable-prof,
 libghc-scientific-dev (>= 0.3),
 libghc-scientific-dev (<< 0.4),
 libghc-scientific-prof,
 libghc-tagged-dev (>= 0.7.3),
 libghc-tagged-dev (<< 0.9),
 libghc-tagged-prof,
 libghc-time-locale-compat-dev (>= 0.1.0.1),
 libghc-time-locale-compat-dev (<< 0.2),
 libghc-time-locale-compat-prof,
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.10),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-quickcheck2-dev (>= 2.10),
 libghc-quickcheck2-dev (<< 2.14),
 libghc-quickcheck2-prof,
 libghc-aeson-dev,
 libghc-attoparsec-dev,
 libghc-base-compat-dev,
 libghc-base-orphans-dev (>= 0.4.5),
 libghc-base-orphans-dev (<< 0.9),
 libghc-base-orphans-prof,
 libghc-exceptions-dev,
 libghc-hashable-dev,
 libghc-quickcheck-instances-dev (>= 0.3.16),
 libghc-quickcheck-instances-dev (<< 0.4),
 libghc-scientific-dev,
 libghc-tagged-dev,
 libghc-tasty-dev (>= 0.10),
 libghc-tasty-dev (<< 1.3),
 libghc-tasty-prof,
 libghc-tasty-hunit-dev (>= 0.9),
 libghc-tasty-hunit-dev (<< 0.11),
 libghc-tasty-hunit-prof,
 libghc-tasty-quickcheck-dev (>= 0.8),
 libghc-tasty-quickcheck-dev (<< 0.11),
 libghc-tasty-quickcheck-prof,
 libghc-time-locale-compat-dev,
 libghc-unordered-containers-dev,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-attoparsec-iso8601-doc,
 libghc-base-compat-doc,
 libghc-exceptions-doc,
 libghc-hashable-doc,
 libghc-scientific-doc,
 libghc-tagged-doc,
 libghc-time-locale-compat-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/phadej/aeson-compat#readme
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-aeson-compat]
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-aeson-compat
X-Description: compatibility layer for aeson
 Compatibility layer for aeson
  * decode etc. work as in aeson >=0.9
  * but it is generalised to work in any MonadThrow (that is extra)
  * .:? works as in aeson ||=0.11
  * .:! works as in aeson ||=0.11 and as .:? did in aeson ==0.10.*
  * Orphan instances FromJSON Day and FromJSON LocalTime for aeson <0.10
  * Encoding related functionality is not added. It's present only with aeson >=0.10

Package: libghc-aeson-compat-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-aeson-compat-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-aeson-compat-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
