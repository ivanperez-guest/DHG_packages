Source: haskell-binary-parsers
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-attoparsec-dev,
 libghc-bytestring-lexing-dev,
 libghc-bytestring-lexing-dev (<< 0.6),
 libghc-bytestring-lexing-dev (>= 0.5),
 libghc-bytestring-lexing-prof,
 libghc-quickcheck-instances-dev (>= 0.3),
 libghc-quickcheck-unicode-dev,
 libghc-quickcheck2-dev (>= 2.7),
 libghc-scientific-dev,
 libghc-scientific-dev (>> 0.3),
 libghc-scientific-prof,
 libghc-semigroups-dev (<< 0.19),
 libghc-semigroups-dev (>= 0.16.1),
 libghc-tasty-dev (>= 0.11),
 libghc-tasty-hunit-dev (>= 0.8),
 libghc-tasty-quickcheck-dev (>= 0.8),
 libghc-unordered-containers-dev,
 libghc-vector-dev,
Build-Depends-Indep:
 ghc-doc,
 libghc-bytestring-lexing-doc,
 libghc-scientific-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/winterland1989/binary-parsers
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-binary-parsers
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-binary-parsers]
X-Description: parsec-/attoparsec-style parsing combinators
 This package extends binary with parsec/attoparsec style parsing
 combinators. It's useful when you want to deal with various binary
 format, and it's very fast. You can now write more complex Binary
 instances using comprehensive combinators, with serialisation
 packages like blaze-texual.
 .
 Binary's Get monad is designed to perform best on non-backtracking
 cases, but it still provides fast backtracking support via
 Alternative instance, it's overall an excellent alternative to
 attoparsec if you only deal with ByteString.

Package: libghc-binary-parsers-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-binary-parsers-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-binary-parsers-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
