Source: haskell-concurrent-output
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-ansi-terminal-dev (>= 0.6.0),
 libghc-ansi-terminal-dev (<< 0.9.0),
 libghc-ansi-terminal-prof,
 libghc-async-dev (>= 2.0),
 libghc-async-dev (<< 2.3),
 libghc-async-prof,
 libghc-exceptions-dev (>= 0.6.0),
 libghc-exceptions-dev (<< 0.11.0),
 libghc-exceptions-prof,
 libghc-terminal-size-dev (<< 0.4.0),
 libghc-terminal-size-dev (>= 0.3.0),
 libghc-terminal-size-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-ansi-terminal-doc,
 libghc-async-doc,
 libghc-exceptions-doc,
 libghc-terminal-size-doc,
Standards-Version: 4.1.4
Homepage: http://hackage.haskell.org/package/concurrent-output
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-concurrent-output
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-concurrent-output]
X-Description: ungarble output from several threads
 Provides a simple interface for writing concurrent programs that
 need to output a lot of status messages to the console, or display
 multiple progress bars for different activities at the same time,
 or concurrently run external commands that output to the console.
 .
 Built on top of that is a way of defining multiple output regions,
 which are automatically laid out on the screen and can be individually
 updated. Can be used for progress displays etc.

Package: libghc-concurrent-output-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-concurrent-output-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-concurrent-output-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
