Source: haskell-cprng-aes
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-byteable-dev,
 libghc-byteable-prof,
 libghc-cipher-aes-dev (<< 0.3),
 libghc-cipher-aes-dev (>= 0.2.9),
 libghc-cipher-aes-prof,
 libghc-crypto-random-dev (<< 0.1),
 libghc-crypto-random-dev (>= 0.0.7),
 libghc-crypto-random-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-byteable-doc,
 libghc-cipher-aes-doc,
 libghc-crypto-random-doc,
Standards-Version: 4.1.4
Homepage: http://github.com/vincenthz/hs-cprng-aes
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-cprng-aes
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-cprng-aes]

Package: libghc-cprng-aes-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: pseudo-random number generator using AES in counter mode${haskell:ShortBlurb}
 It contains a simple crypto pseudo-random-number-generator with
 really good randomness property.
 .
 Using ent, a randomness property maker on one 1Mb sample:
 Entropy = 7.999837 bits per byte. Optimum compression would reduce
 the size of this 1048576 byte file by 0 percent. Chi square distribution
 for 1048576 samples is 237.02 Arithmetic mean value of data bytes is
 127.3422 (127.5 = random) Monte Carlo value for Pi is 3.143589568
 (error 0.06 percent).
 .
 Compared to urandom with the same sampling: Entropy = 7.999831 bits per
 byte. Optimum compression would reduce the size of this 1048576 byte file
 by 0 percent. Chi square distribution for 1048576 samples is 246.63
 Arithmetic mean value of data bytes is 127.6347 (127.5 = random). Monte
 Carlo value for Pi is 3.132465868 (error 0.29 percent).
 .
 ${haskell:Blurb}

Package: libghc-cprng-aes-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: pseudo-random number generator using AES in counter mode${haskell:ShortBlurb}
 It contains a simple crypto pseudo-random-number-generator with
 really good randomness property.
 .
 Using ent, a randomness property maker on one 1Mb sample:
 Entropy = 7.999837 bits per byte. Optimum compression would reduce
 the size of this 1048576 byte file by 0 percent. Chi square distribution
 for 1048576 samples is 237.02 Arithmetic mean value of data bytes is
 127.3422 (127.5 = random) Monte Carlo value for Pi is 3.143589568
 (error 0.06 percent).
 .
 Compared to urandom with the same sampling: Entropy = 7.999831 bits per
 byte. Optimum compression would reduce the size of this 1048576 byte file
 by 0 percent. Chi square distribution for 1048576 samples is 246.63
 Arithmetic mean value of data bytes is 127.6347 (127.5 = random). Monte
 Carlo value for Pi is 3.132465868 (error 0.29 percent).
 .
 ${haskell:Blurb}

Package: libghc-cprng-aes-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: pseudo-random number generator using AES in counter mode${haskell:ShortBlurb}
  It contains a simple crypto pseudo-random-number-generator with
 really good randomness property.
 .
 Using ent, a randomness property maker on one 1Mb sample:
 Entropy = 7.999837 bits per byte. Optimum compression would reduce
 the size of this 1048576 byte file by 0 percent. Chi square distribution
 for 1048576 samples is 237.02 Arithmetic mean value of data bytes is
 127.3422 (127.5 = random) Monte Carlo value for Pi is 3.143589568
 (error 0.06 percent).
 .
 Compared to urandom with the same sampling: Entropy = 7.999831 bits per
 byte. Optimum compression would reduce the size of this 1048576 byte file
 by 0 percent. Chi square distribution for 1048576 samples is 246.63
 Arithmetic mean value of data bytes is 127.6347 (127.5 = random). Monte
 Carlo value for Pi is 3.132465868 (error 0.29 percent).
 .
 ${haskell:Blurb}
