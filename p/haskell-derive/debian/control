Source: haskell-derive
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-src-exts-dev (>= 1.20),
 libghc-src-exts-dev (<< 1.21),
 libghc-src-exts-prof,
 libghc-syb-dev,
 libghc-syb-prof,
 libghc-uniplate-dev (<< 1.7),
 libghc-uniplate-dev (>= 1.5),
 libghc-uniplate-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-src-exts-doc,
 libghc-syb-doc,
 libghc-uniplate-doc,
Standards-Version: 4.1.4
Homepage: http://community.haskell.org/~ndm/derive/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-derive
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-derive]

Package: libghc-derive-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Deriving instances for data types in Haskell${haskell:ShortBlurb}
 Data.Derive is a library and a tool for deriving instances for Haskell
 programs.  It is designed to work with custom derivations, SYB and
 Template Haskell mechanisms.  The tool requires GHC, but the generated
 code is portable to all compilers.  This tool can be regarded as a
 competitor to DrIFT.
 .
 ${haskell:Blurb}

Package: libghc-derive-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Deriving instances for data types in Haskell${haskell:ShortBlurb}
 Data.Derive is a library and a tool for deriving instances for Haskell
 programs.  It is designed to work with custom derivations, SYB and
 Template Haskell mechanisms.  The tool requires GHC, but the generated
 code is portable to all compilers.  This tool can be regarded as a
 competitor to DrIFT.
 .
 ${haskell:Blurb}

Package: libghc-derive-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Deriving instances for data types in Haskell${haskell:ShortBlurb}
 Data.Derive is a library and a tool for deriving instances for Haskell
 programs.  It is designed to work with custom derivations, SYB and
 Template Haskell mechanisms.  The tool requires GHC, but the generated
 code is portable to all compilers.  This tool can be regarded as a
 competitor to DrIFT.
 .
 ${haskell:Blurb}

Package: haskell-derive-utils
Architecture: any
Section: misc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Deriving instances for data types in Haskell
 Data.Derive is a library and a tool for deriving instances for Haskell
 programs.  It is designed to work with custom derivations, SYB and
 Template Haskell mechanisms.  The tool requires GHC, but the generated
 code is portable to all compilers.  This tool can be regarded as a
 competitor to DrIFT.
 .
 ${haskell:Blurb}
