haskell-ekg (0.4.0.15-5) unstable; urgency=medium

  * Bump dependency bounds

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 06 Sep 2019 20:10:08 +0200

haskell-ekg (0.4.0.15-4) unstable; urgency=medium

  * Newer build-deps from hackage

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 05 Nov 2018 22:49:55 +0200

haskell-ekg (0.4.0.15-3) unstable; urgency=medium

  * Remove build dependency on libghc-text-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:47:29 +0300

haskell-ekg (0.4.0.15-2) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:07:37 +0300

haskell-ekg (0.4.0.15-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 15:53:34 -0400

haskell-ekg (0.4.0.14-1) unstable; urgency=medium

  [ Sean Whitton ]
  * New upstream release

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sat, 04 Nov 2017 20:53:32 +0200

haskell-ekg (0.4.0.13-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sat, 17 Jun 2017 14:27:39 -0400

haskell-ekg (0.4.0.11-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:32 -0400

haskell-ekg (0.4.0.11-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 17:03:52 -0400

haskell-ekg (0.4.0.11-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 20 Sep 2016 14:23:21 -0400

haskell-ekg (0.4.0.10-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Joachim Breitner ]
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 01 Jun 2016 12:47:10 +0200

haskell-ekg (0.4.0.8-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:24 -0500

haskell-ekg (0.4.0.8-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:32 +0200

haskell-ekg (0.4.0.5-3) unstable; urgency=medium

  * Rebuild due to haskell-devscripts bug affecting the previous

 -- Joachim Breitner <nomeata@debian.org>  Tue, 28 Apr 2015 23:58:31 +0200

haskell-ekg (0.4.0.5-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:26 +0200

haskell-ekg (0.4.0.5-1) experimental; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Tue, 23 Dec 2014 13:53:48 +0100

haskell-ekg (0.3.1.4-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental
  * Do not depend on transformers, which now comes with GHC

 -- Joachim Breitner <nomeata@debian.org>  Sun, 21 Dec 2014 19:47:04 +0100

haskell-ekg (0.3.1.4-1) unstable; urgency=low

  [ Joachim Breitner ]
  * Adjust watch file to new hackage layout

  [ Raúl Benencia ]
  * New upstream release
  * Bump standards version, no change

 -- Raúl Benencia <rul@kalgan.cc>  Wed, 09 Apr 2014 21:09:18 -0300

haskell-ekg (0.3.1.3-1) unstable; urgency=low

  [ Louis Bettens ]
  * /usr/share/ekg-0.3.1.2 -> /usr/share/ekg
  * Bump version of Build-Depends: on haskell-devscripts

  [ Clint Adams ]
  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 03 Aug 2013 13:45:40 -0400

haskell-ekg (0.3.1.2-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:32 +0200

haskell-ekg (0.3.1.2-1) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 02 Dec 2012 21:52:47 +0100

haskell-ekg (0.3.1.0-1) unstable; urgency=low

  * New upstream release, adding the 'Label' type

 -- Iustin Pop <iustin@debian.org>  Sun, 20 May 2012 14:15:01 +0200

haskell-ekg (0.3.0.5-1) unstable; urgency=low

  * Initial release. (Closes: #665911)

 -- Iustin Pop <iustin@debian.org>  Mon, 16 Apr 2012 21:38:21 +0200
