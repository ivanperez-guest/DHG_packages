haskell-gnutls (0.2-6) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:09:57 +0300

haskell-gnutls (0.2-5) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:50 -0400

haskell-gnutls (0.2-4) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:53 -0400

haskell-gnutls (0.2-3) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Fri, 14 Oct 2016 16:02:32 -0400

haskell-gnutls (0.2-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:30 -0500

haskell-gnutls (0.2-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:43 +0200

haskell-gnutls (0.1.4-8) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:59 +0200

haskell-gnutls (0.1.4-7) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental
  * Do not depend on transformers, which now comes with GHC

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 20:31:47 +0100

haskell-gnutls (0.1.4-6) unstable; urgency=medium

  * Correct libghc-gnutls-dev's GnuTLS dependency (libgnutls28-dev, instead
  of
    libgnutls-dev). Closes: #754561
    This preempts an NMU by Andreas Metzler. Thanks for that!

 -- Joachim Breitner <nomeata@debian.org>  Sat, 06 Sep 2014 19:34:16 +0200

haskell-gnutls (0.1.4-5) unstable; urgency=medium

  [ Joachim Breitner ]
  * Adjust watch file to new hackage layout

  [ Clint Adams ]
  * Rebuild against libgnutls28-dev.  closes: #753094.

 -- Clint Adams <clint@debian.org>  Sun, 29 Jun 2014 09:34:37 -0400

haskell-gnutls (0.1.4-3) unstable; urgency=low

  * Enable compat level 9
  * Bump standards version to 3.9.4

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:44 +0200

haskell-gnutls (0.1.4-2) experimental; urgency=low

  [ Joachim Breitner ]
  * Depend on haskell-devscripts 0.8.13 to ensure this package is built
    against experimental
  * Bump standards version, no change

 -- Clint Adams <clint@debian.org>  Sun, 11 Nov 2012 14:48:53 -0500

haskell-gnutls (0.1.4-1) unstable; urgency=low

  * New upstream release, fixing a segmentation fault due to a lost
    reference to the credentials object.

 -- Joey Hess <joeyh@debian.org>  Fri, 26 Oct 2012 22:52:28 -0400

haskell-gnutls (0.1.2-1) unstable; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Thu, 17 May 2012 23:45:43 +0200

haskell-gnutls (0.1.1-2) unstable; urgency=low

  * Fix -dev dependency on libgnutls-dev.

 -- Clint Adams <clint@debian.org>  Mon, 12 Mar 2012 19:18:14 -0400

haskell-gnutls (0.1.1-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Wed, 07 Mar 2012 18:01:23 -0500
