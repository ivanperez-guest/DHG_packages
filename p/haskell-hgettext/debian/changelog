haskell-hgettext (0.1.31.0-5) unstable; urgency=medium

  * debian/patches/16.patch: Upstream fix for new cabal 2.4
  * Patch for new containers

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 28 Aug 2019 13:43:14 +0200

haskell-hgettext (0.1.31.0-4) unstable; urgency=medium

  * Fixup testsuite, by removing a ":" on cabal file

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 17 Dec 2018 10:35:58 +0100

haskell-hgettext (0.1.31.0-3) unstable; urgency=medium

  * Newer build-deps from hackage

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 15 Oct 2018 16:33:06 +0300

haskell-hgettext (0.1.31.0-2) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:10:57 +0300

haskell-hgettext (0.1.31.0-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 15 Apr 2018 11:07:05 -0400

haskell-hgettext (0.1.30-16) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:52 -0400

haskell-hgettext (0.1.30-15) unstable; urgency=medium

  * Patch for haskell-src-exts 1.18.  closes: #867324.

 -- Clint Adams <clint@debian.org>  Fri, 07 Jul 2017 20:03:08 -0400

haskell-hgettext (0.1.30-14) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:34:04 -0400

haskell-hgettext (0.1.30-13) experimental; urgency=medium

  * Drop cabal from Build-Depends.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 01:19:24 -0400

haskell-hgettext (0.1.30-12) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 01:10:01 -0400

haskell-hgettext (0.1.30-11) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:33 -0500

haskell-hgettext (0.1.30-10) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:47 +0200

haskell-hgettext (0.1.30-9) unstable; urgency=medium

  * Do not set TemplateHaskell in *.cabal, it is not used.

 -- Joachim Breitner <nomeata@debian.org>  Mon, 15 Jun 2015 22:30:36 +0200

haskell-hgettext (0.1.30-8) unstable; urgency=medium

  * Rebuild due to haskell-devscripts bug affecting the previous

 -- Joachim Breitner <nomeata@debian.org>  Tue, 28 Apr 2015 23:58:36 +0200

haskell-hgettext (0.1.30-7) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:50:19 +0200

haskell-hgettext (0.1.30-6) experimental; urgency=low

  * Depends on haskell-devscripts 0.9, found in experimental
    (was accidentally uploaded to experimental with dependency on 0.8)

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Mon, 12 Jan 2015 17:56:44 +0100

haskell-hgettext (0.1.30-5) unstable; urgency=high

  * Same version as 0.1.30-4, but for unstable
  * Fixes incompatibilities with cabal-install when hgettext and
    cabal-install by ensuring that they are built against the same version
    of libghc-cabal-dev. For more detail look at he bug report #774802.

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Fri, 09 Jan 2015 18:55:39 +0100

haskell-hgettext (0.1.30-4) experimental; urgency=high

  [ Sven Bartscher ]
  * Assure that hgettext is built against the right version of cabal.

 -- Joachim Breitner <nomeata@debian.org>  Wed, 07 Jan 2015 16:18:45 +0100

haskell-hgettext (0.1.30-3) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:10:49 +0100

haskell-hgettext (0.1.30-2) unstable; urgency=medium

  * Correct wrong VCS-Darcs field
  * Add Build-Dependency on libghc-cabal-dev to assure campatibility with
    cabal-install

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Fri, 03 Oct 2014 17:53:47 +0200

haskell-hgettext (0.1.30-1) unstable; urgency=low

  * Initial release.

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Sun, 18 May 2014 16:35:17 +0200
