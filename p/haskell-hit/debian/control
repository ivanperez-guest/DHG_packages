Source: haskell-hit
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-attoparsec-dev (>= 0.10.1),
 libghc-attoparsec-prof,
 libghc-byteable-dev,
 libghc-byteable-prof,
 libghc-bytedump-dev (>= 1.0),
 libghc-cryptohash-dev,
 libghc-cryptohash-prof,
 libghc-hourglass-dev,
 libghc-hourglass-dev (>= 0.2),
 libghc-hourglass-prof,
 libghc-patience-dev,
 libghc-patience-prof,
 libghc-random-dev,
 libghc-random-prof,
 libghc-system-fileio-dev,
 libghc-system-fileio-prof,
 libghc-system-filepath-dev,
 libghc-system-filepath-prof,
 libghc-tasty-dev,
 libghc-tasty-quickcheck-dev,
 libghc-unix-compat-dev,
 libghc-unix-compat-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libghc-vector-dev,
 libghc-vector-prof,
 libghc-zlib-bindings-dev (<< 0.2),
 libghc-zlib-bindings-dev (>= 0.1),
 libghc-zlib-bindings-prof,
 libghc-zlib-dev,
 libghc-zlib-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-attoparsec-doc,
 libghc-byteable-doc,
 libghc-cryptohash-doc,
 libghc-hourglass-doc,
 libghc-patience-doc,
 libghc-random-doc,
 libghc-system-fileio-doc,
 libghc-system-filepath-doc,
 libghc-unix-compat-doc,
 libghc-utf8-string-doc,
 libghc-vector-doc,
 libghc-zlib-bindings-doc,
 libghc-zlib-doc,
Standards-Version: 4.1.4
Homepage: http://github.com/vincenthz/hit
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-hit
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-hit]
X-Description: Git operations in Haskell
 A Haskell implementation of git storage operations, allowing users to
 manipulate git repositories (read and write).
 .
 This implementation is fully interoperable with the main C
 implementation.
 .
 This is strictly only manipulating the git store (what's inside
 the .git directory), and doesn't do anything with the index or
 your working directory files.

Package: libghc-hit-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-hit-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-hit-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
