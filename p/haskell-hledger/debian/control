Source: haskell-hledger
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-ghci,
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-decimal-dev,
 libghc-decimal-prof,
 libghc-diff-dev,
 libghc-diff-prof,
 libghc-ansi-terminal-dev (>= 0.6.2.3),
 libghc-ansi-terminal-prof,
 libghc-base-compat-batteries-dev (>= 0.10.1),
 libghc-base-compat-batteries-dev (<< 0.11),
 libghc-base-compat-batteries-prof,
 libghc-cmdargs-dev (>= 0.10),
 libghc-cmdargs-prof,
 libghc-data-default-dev (>= 0.5),
 libghc-data-default-prof,
 libghc-easytest-dev,
 libghc-easytest-prof,
 libghc-file-embed-dev (>= 0.0.10),
 libghc-file-embed-prof,
 libghc-hashable-dev (>= 1.2.4),
 libghc-hashable-prof,
 libghc-here-dev,
 libghc-here-prof,
 libghc-hledger-lib-dev (>= 1.12),
 libghc-hledger-lib-dev (<< 1.13),
 libghc-hledger-lib-prof,
 libghc-lucid-dev,
 libghc-lucid-prof,
 libghc-math-functions-dev (>= 0.2.0.0),
 libghc-math-functions-prof,
 libghc-megaparsec-dev (>= 7.0.0),
 libghc-megaparsec-dev (<< 8),
 libghc-megaparsec-prof,
 libghc-old-time-dev,
 libghc-old-time-prof,
 libghc-pretty-show-dev (>= 1.6.4),
 libghc-pretty-show-prof,
 libghc-regex-tdfa-dev,
 libghc-regex-tdfa-prof,
 libghc-safe-dev (>= 0.2),
 libghc-safe-prof,
 libghc-shakespeare-dev (>= 2.0.2.2),
 libghc-shakespeare-prof,
 libghc-split-dev (>= 0.1),
 libghc-split-prof,
 libghc-tabular-dev (>= 0.2),
 libghc-tabular-prof,
 libghc-temporary-dev,
 libghc-temporary-prof,
 libghc-unordered-containers-dev,
 libghc-unordered-containers-prof,
 libghc-utf8-string-dev (>= 0.3.5),
 libghc-utf8-string-prof,
 libghc-utility-ht-dev (>= 0.0.13),
 libghc-utility-ht-prof,
 libghc-wizards-dev (>= 1.0),
 libghc-wizards-prof,
Build-Depends-Indep: ghc-doc,
 libghc-decimal-doc,
 libghc-diff-doc,
 libghc-ansi-terminal-doc,
 libghc-base-compat-batteries-doc,
 libghc-cmdargs-doc,
 libghc-data-default-doc,
 libghc-easytest-doc,
 libghc-file-embed-doc,
 libghc-hashable-doc,
 libghc-here-doc,
 libghc-hledger-lib-doc,
 libghc-lucid-doc,
 libghc-math-functions-doc,
 libghc-megaparsec-doc,
 libghc-old-time-doc,
 libghc-pretty-show-doc,
 libghc-regex-tdfa-doc,
 libghc-safe-doc,
 libghc-shakespeare-doc,
 libghc-split-doc,
 libghc-tabular-doc,
 libghc-temporary-doc,
 libghc-unordered-containers-doc,
 libghc-utf8-string-doc,
 libghc-utility-ht-doc,
 libghc-wizards-doc,
Standards-Version: 4.4.0
Homepage: https://hledger.org
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-hledger
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-hledger]

Package: hledger
Architecture: any
Multi-Arch: foreign
Section: utils
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: command-line double-entry accounting program
 hledger is a Haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides the main hledger command-line
 tool; see the other hledger-* packages for web and curses interfaces
 and chart generation. hledger aims to be a reliable, practical
 financial reporting tool for day-to-day use, and also a useful
 library for building financial apps in haskell. Given a plain text
 file describing transactions, of money or any other commodity,
 .
 hledger will print the chart of accounts, account balances, or
 transactions you're interested in. It can also help you add
 transactions to the journal file, or convert CSV data from your bank.

Package: libghc-hledger-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: CLI libraries for hledger${haskell:ShortBlurb}
 hledger is a Haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides the main hledger command-line
 tool; see the other hledger-* packages for web and curses interfaces
 and chart generation. hledger aims to be a reliable, practical
 financial reporting tool for day-to-day use, and also a useful
 library for building financial apps in haskell. Given a plain text
 file describing transactions, of money or any other commodity,
 .
 hledger will print the chart of accounts, account balances, or
 transactions you're interested in. It can also help you add
 transactions to the journal file, or convert CSV data from your bank.
 .
 ${haskell:Blurb}

Package: libghc-hledger-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: CLI libraries for hledger${haskell:ShortBlurb}
 hledger is a Haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides the main hledger command-line
 tool; see the other hledger-* packages for web and curses interfaces
 and chart generation. hledger aims to be a reliable, practical
 financial reporting tool for day-to-day use, and also a useful
 library for building financial apps in haskell. Given a plain text
 file describing transactions, of money or any other commodity,
 .
 hledger will print the chart of accounts, account balances, or
 transactions you're interested in. It can also help you add
 transactions to the journal file, or convert CSV data from your bank.
 .
 ${haskell:Blurb}

Package: libghc-hledger-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: CLI libraries for hledger${haskell:ShortBlurb}
 hledger is a Haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides the main hledger command-line
 tool; see the other hledger-* packages for web and curses interfaces
 and chart generation. hledger aims to be a reliable, practical
 financial reporting tool for day-to-day use, and also a useful
 library for building financial apps in haskell. Given a plain text
 file describing transactions, of money or any other commodity,
 .
 hledger will print the chart of accounts, account balances, or
 transactions you're interested in. It can also help you add
 transactions to the journal file, or convert CSV data from your bank.
 .
 ${haskell:Blurb}
