Source: haskell-persistent-postgresql
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.6.2),
 libghc-aeson-prof,
 libghc-blaze-builder-dev,
 libghc-blaze-builder-prof,
 libghc-conduit-dev (>= 1.2.8),
 libghc-conduit-prof,
 libghc-monad-logger-dev (>= 0.3.4),
 libghc-monad-logger-prof,
 libghc-persistent-dev (>= 2.9),
 libghc-persistent-dev (<< 3),
 libghc-persistent-prof,
 libghc-postgresql-libpq-dev (<< 0.10),
 libghc-postgresql-libpq-dev (>= 0.6.1),
 libghc-postgresql-libpq-prof,
 libghc-postgresql-simple-dev (>= 0.4.0),
 libghc-postgresql-simple-dev (<< 0.7),
 libghc-postgresql-simple-prof,
 libghc-resource-pool-dev,
 libghc-resource-pool-prof,
 libghc-resourcet-dev (>= 1.1),
 libghc-resourcet-prof,
 libghc-unliftio-core-dev,
 libghc-unliftio-core-prof,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-blaze-builder-doc,
 libghc-conduit-doc,
 libghc-monad-logger-doc,
 libghc-persistent-doc,
 libghc-postgresql-libpq-doc,
 libghc-postgresql-simple-doc,
 libghc-resource-pool-doc,
 libghc-resourcet-doc,
 libghc-unliftio-core-doc,
Standards-Version: 4.4.0
Homepage: http://www.yesodweb.com/book/persistent
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-persistent-postgresql
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-persistent-postgresql]

Package: libghc-persistent-postgresql-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: backend for the persistent library using PostgreSQL
 Based on the postgresql-simple package
 .
  Author: Felipe Lessa, Michael Snoyman <michael@snoyman.com>
  Upstream-Maintainer: Michael Snoyman <michael@snoyman.com>
 .
 This package contains the normal library files.

Package: libghc-persistent-postgresql-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: backend for the persistent library using PostgreSQL; profiling libraries
 Based on the postgresql-simple package
 .
  Author: Felipe Lessa, Michael Snoyman <michael@snoyman.com>
  Upstream-Maintainer: Michael Snoyman <michael@snoyman.com>
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-persistent-postgresql-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: backend for the persistent library using PostgreSQL; documentation
 Based on the postgresql-simple package
 .
  Author: Felipe Lessa, Michael Snoyman <michael@snoyman.com>
  Upstream-Maintainer: Michael Snoyman <michael@snoyman.com>
 .
 This package contains the documentation files.
