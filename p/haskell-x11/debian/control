Source: haskell-x11
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
 Ryan Kavanagh <rak@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-data-default-dev,
 libghc-data-default-prof,
 libx11-dev,
 libxext-dev,
 libxinerama-dev,
 libxrandr-dev,
 libxt-dev,
 libxss-dev,
Build-Depends-Indep:
 ghc-doc,
 libghc-data-default-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/xmonad/X11
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-x11
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-x11]

Package: libghc-x11-dev
Architecture: any
Depends:
 libx11-dev,
 libxinerama-dev,
 libxrandr-dev,
 libxt-dev,
 libxss-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell X11 binding for GHC${haskell:ShortBlurb}
 This library is a binding to the X11 graphics library.
 The binding is a direct translation of the C binding; for
 documentation of these calls, refer to "The Xlib Programming
 Manual", available online at <http://tronche.com/gui/x/xlib/>.
 .
 ${haskell:Blurb}

Package: libghc-x11-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Provides:
 ${haskell:Provides},
Description: Haskell X11 binding for GHC${haskell:ShortBlurb}
 This library is a binding to the X11 graphics library.
 The binding is a direct translation of the C binding; for
 documentation of these calls, refer to "The Xlib Programming
 Manual", available online at <http://tronche.com/gui/x/xlib/>.
 .
 ${haskell:Blurb}

Package: libghc-x11-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell X11 binding for GHC${haskell:ShortBlurb}
 This library is a binding to the X11 graphics library.
 The binding is a direct translation of the C binding; for
 documentation of these calls, refer to "The Xlib Programming
 Manual", available online at <http://tronche.com/gui/x/xlib/>.
 .
 ${haskell:Blurb}
