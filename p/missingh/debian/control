Source: missingh
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 John Goerzen <jgoerzen@complete.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-hslogger-dev,
 libghc-hslogger-prof,
 libghc-hunit-dev,
 libghc-hunit-prof,
 libghc-network-dev,
 libghc-network-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-old-time-dev,
 libghc-old-time-prof,
 libghc-random-dev,
 libghc-random-prof,
 libghc-regex-compat-dev,
 libghc-regex-compat-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-hslogger-doc,
 libghc-hunit-doc,
 libghc-network-doc,
 libghc-old-locale-doc,
 libghc-old-time-doc,
 libghc-random-doc,
 libghc-regex-compat-doc,
Standards-Version: 4.1.4
Homepage: http://software.complete.org/missingh
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/missingh
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/missingh]

Package: libghc-missingh-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Library of utility functions for Haskell
 MissingH is a library of all sorts of utility functions for
 Haskell programmers.  It is written in pure Haskell and thus should
 be extremely portable and easy to use.  It also has no prerequisites
 save those that are commonly included with Haskell compilers.
 .
 MissingH is based on MissingLib library for OCaml and contains some
 of the same features.  However, some features are left behind because
 they are already in Haskell or not needed here -- and others are added
 due to things Haskell is missing, or things that Haskell makes
 possible.

Package: libghc-missingh-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Library of utility functions for Haskell, profiling libraries
 MissingH is a library of all sorts of utility functions for
 Haskell programmers.  It is written in pure Haskell and thus should
 be extremely portable and easy to use.  It also has no prerequisites
 save those that are commonly included with Haskell compilers.
 .
 MissingH is based on MissingLib library for OCaml and contains some
 of the same features.  However, some features are left behind because
 they are already in Haskell or not needed here -- and others are added
 due to things Haskell is missing, or things that Haskell makes
 possible.
 .
 This package provides the MissingH libraries compiled for profiling.

Package: libghc-missingh-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Breaks:
 missingh-doc (<< 1.1.0.3-2),
Provides:
 missingh-doc,
Replaces:
 missingh-doc (<< 1.1.0.3-2),
Description: Documentation for Haskell utility library
 MissingH is a library of all sorts of utility functions for
 Haskell programmers.  It is written in pure Haskell and thus should
 be extremely portable and easy to use.  It also has no prerequisites
 save those that are commonly included with Haskell compilers.
 .
 MissingH is based on MissingLib library for OCaml and contains some
 of the same features.  However, some features are left behind because
 they are already in Haskell or not needed here -- and others are added
 due to things Haskell is missing, or things that Haskell makes
 possible.
 .
 This package provides the API documentation for MissingH.
